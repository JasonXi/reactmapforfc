import { Map, Marker } from 'react-amap'
import * as React from 'react'

export class BasicMap extends React.Component<any, any> {
    renderBasic = () => {
        return <div style={{width: '100%', height: '400px'}}>
            <Map amapkey={'788e08def03f95c670944fe2c78fa76f'}/>
        </div>
    }
    renderBasicWithPlugins = () => {
        const plugins: any[] = [
            'MapType', //地图是2d图还是卫星图
            'Scale', // 缩放
            'OverView',
            //'ControlBar', // similar with tool bar
            {
                name: 'ToolBar',
                options: {
                    visible: true,  // 不设置该属性默认就是 true
                    onCreated(ins: any) {
                        console.log('toolbar:', ins)
                    }
                }
            }
        ]
        return <div style={{width: '400px', height: '400px'}}>
            <Map
                amapkey={'788e08def03f95c670944fe2c78fa76f'}
                plugins={plugins}
            />
        </div>
    }

    renderBasicWithCenter = () => {
        const center = {
            longitude: 119.490353,
            latitude: 31.186472
        }
        return <div style={{width: '400px', height: '400px'}}>
            <Map
                amapkey={'788e08def03f95c670944fe2c78fa76f'}
                center={center}
            />
        </div>
    }

    renderBasicWithAMapUI = () => {
        const center = {
            longitude: 119.490353,
            latitude: 31.186472
        }
        const markPosition = {longitude: 119, latitude: 31}
        return <div style={{width: '400px', height: '400px'}}>
            <Map
                amapkey={'788e08def03f95c670944fe2c78fa76f'}
                center={center}
            />
            <Marker position={markPosition}/>
        </div>
    }

    render(): React.ReactNode {
        //return this.renderBasicWithPlugins()
        //return this.renderBasicWithCenter()
        return this.renderBasicWithAMapUI()
    }
}

