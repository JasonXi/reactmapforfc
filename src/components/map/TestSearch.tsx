import { Map, Marker } from 'react-amap'
import * as React from 'react'
import { Input, Select, Spin } from 'antd'
import {debounce} from 'lodash'

const { Option } = Select

class SearchComponent extends React.Component<any, any> {
    private aMap: any
    private autoComplete: any
    private lastFetchId: number
    constructor(props: any) {
        super(props)
        const aMap = this.props.__map__
        if(!aMap){
            console.log('make sure the component is child of Map')
        }
        this.aMap = aMap
        aMap.plugin(['AMap.Autocomplete'], () => {
            const autoOptions = {
                city: '全国'
            }
            this.autoComplete = new window.AMap.Autocomplete(autoOptions)
        })
        this.state = {
            searchValue: '',
            data: [],
            value: [],
            fetching: false
        }
        this.lastFetchId = 0
        this.fetchUser = debounce(this.fetchUser, 800)
    }

    handleChange = (value: any, option: any) => {
        this.setState({
            value,
            data: [],
            fetching: false,
        })
        for(let i=0;i<option.length; i++) {
            const oneOption = option[i].key
            const lat = oneOption.split(':')[0]
            const lng = oneOption.split(':')[1]

            const location = {
                latitude: lat,
                longitude: lng
            }
            console.log(location)
            this.props.changePosition(location)
        }
    }

    fetchUser = (value: any) => {
        console.log('fetching user', value)
        this.lastFetchId += 1
        const fetchId = this.lastFetchId
        this.setState({ data: [], fetching: true })
        this.autoComplete.search(value, (status: any, result: any) => {
            console.log('result:', result)
            if (fetchId !== this.lastFetchId) {
                // for fetch callback order
                return
            }
            const data = result.tips.map((address: any) => ({
                text: address.name,
                value: address.location.lat+':'+address.location.lng
            }))
            this.setState({
                data: data,
                fetching: false
            })
        })

    }
    render(): React.ReactNode {
        const { fetching, data, value } = this.state
        return (
            <>
            <Select
                mode="multiple"
                labelInValue
                value={value}
                placeholder="Select place"
                notFoundContent={fetching ? <Spin size="small" /> : null}
                filterOption={false}
                onSearch={this.fetchUser}
                onChange={this.handleChange}
                style={{ width: '100%' }}
            >
                {data.map((d: any) => (
                    <Option key={d.value}>{d.text}</Option>
                ))}
            </Select>

            </>
        )
    }
}
export class TestSearch extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            position: {
                longitude: 119.49,
                latitude: 31.1
            }
        }
    }
    changePosition = (position: any) => {
        this.setState({
            position: position
        })
    }
    render(): React.ReactNode {
        return <div style={{width: '400px', height: '400px'}}>
            <Map>
                <Marker position={this.state.position}>
                    <div style={{width: '60px', height:'20px', border: '1px solid red', background: '#d4dce1'}}>YUXIXI</div>
                </Marker>
                <SearchComponent position={this.state.position} changePosition={this.changePosition}/>
            </Map>
        </div>
    }
}
