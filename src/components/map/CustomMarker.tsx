import { Map, Marker } from 'react-amap'
import * as React from 'react'
export class CustomMarker extends React.Component<any, any> {
    private tool: any
    private mapPlugins: any[]
    private markerPosition: { latitude: number; longitude: number }
    private mapCenter: { latitude: number; longitude: number }
    private toolEvents: { created: (tool: any) => void }
    constructor(props: any){
        super(props)
        this.toolEvents = {
            created: (tool: any) => {
                this.tool = tool
            }
        }
        this.mapPlugins = ['ToolBar']
        this.mapCenter = {longitude: 119.490353, latitude: 31.186472}
        this.markerPosition = {longitude: 119.49, latitude: 31.1}
    }

    render(){
        return <div>
            <div style={{width: '400px', height: '400px'}}>
                <Map
                    plugins={this.mapPlugins}
                    center={this.mapCenter}
                >
                    <Marker position={this.markerPosition} />
                </Map>
            </div>
        </div>
    }
}
