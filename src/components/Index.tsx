import * as React from 'react'
import './Index.css'
import './Index.less'
import { observer } from 'mobx-react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { BasicMap } from './map/BasicMap'
import { CustomMarker } from './map/CustomMarker'
import { TestSearch } from './map/TestSearch'

interface IndexState {
    inputValue: string;
}

@observer
export class Index extends React.Component<any, IndexState> {
    constructor(props: any) {
        super(props)
    }

    render(): React.ReactNode {
        return (
            <>
                <Router>
                    <Switch>
                        {/* eslint-disable-next-line react/no-children-prop */}
                        <Route exact path='/'>
                            just tell you success.
                        </Route>
                        <Route path='/map/basic_map'>
                            <BasicMap/>
                        </Route>
                        <Route path='/map/marker' component={CustomMarker}/>
                        <Route path='/map/search' component={TestSearch}/>
                    </Switch>
                </Router>
            </>
        )
    }
}
